package com.example.messagingapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MessagingappApplication

fun main(args: Array<String>) {
	runApplication<MessagingappApplication>(*args)
}
