package com.example.messagingapp.messagethread

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(path = ["threads"])
class MessageThreadController(
    val MESSAGETHREAD_SERVICE: MessageThreadService
) {
    /****************
     * GET MAPPINGS *
     ****************/

    @GetMapping
    fun getThreads(
        @RequestParam(required = false) id: Long?,
        @RequestParam(required = false) person1Id: Long?,
        @RequestParam(required = false) person1FName: String?,
        @RequestParam(required = false) person1LName: String?,
        @RequestParam(required = false) person2Id: Long?,
        @RequestParam(required = false) person2FName: String?,
        @RequestParam(required = false) person2LName: String?
    ): List<MessageThread> {
        return MESSAGETHREAD_SERVICE.getThreads(
            id,
            person1Id,
            person1FName,
            person1LName,
            person2Id,
            person2FName,
            person2LName
        )
    }

    /*****************
     * POST MAPPINGS *
     *****************/

    @PostMapping("/create/")
    fun createThread(
        @RequestParam id1: Long,
        @RequestParam id2: Long,
    ) {
        MESSAGETHREAD_SERVICE.createMessageThread(id1, id2)
    }
}