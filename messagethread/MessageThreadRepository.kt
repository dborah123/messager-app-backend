package com.example.messagingapp.messagethread

import com.example.messagingapp.person.Person
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MessageThreadRepository: JpaRepository<MessageThread, Long> {

    @Query("SELECT m FROM MessageThread m WHERE "
    + "(:id = null OR :id = m.id) "
    + "AND (:id1 = null OR :id1 MEMBER OF m.people) "
    + "AND (:id2 = null OR :id2 MEMBER OF m.people)"
    )
    fun getThreads(
        @Param("id") id: Long?,
        @Param("id1") person1Id: Person?,
        @Param("id2") person2Id: Person?,
    ): List<MessageThread>
}