package com.example.messagingapp.messagethread

import com.example.messagingapp.message.Message
import com.example.messagingapp.person.Person
import com.example.messagingapp.person.PersonRepository
import org.springframework.stereotype.Service

@Service
class MessageThreadService(
    val MESSAGETHREAD_REPO: MessageThreadRepository,
    val PERSON_REPO: PersonRepository
) {

    /*****************
     * GET FUNCTIONS *
     *****************/

    fun getThreads(
        id: Long?,
        person1Id: Long?,
        person1FName: String?,
        person1LName: String?,
        person2Id: Long?,
        person2FName: String?,
        person2LName: String?
    ): List<MessageThread> {
        val person1: Person?
        val person2: Person?

        // Getting person1 id if desired
        if (person1Id != null || person1FName != null || person1LName != null) {
            val personQuery = PERSON_REPO.getSpecificPeople(
                person1Id,
                person1FName,
                person1LName
            )

            if (personQuery.size != 1) {
                person1 = null
            } else {
                person1 = personQuery[0]
            }
        } else {
            person1 = null
        }

        // Getting person2 id if desired
        if (person2Id != null || person2FName != null || person2LName != null){
            val personQuery = PERSON_REPO.getSpecificPeople(
                person1Id,
                person2FName,
                person2LName
            )

            if (personQuery.size != 1) {
                person2 = null
            } else {
                person2 = personQuery[0]
            }
        } else {
            person2 = null
        }

        return MESSAGETHREAD_REPO.getThreads(
            id,
            person1,
            person2
        )
    }

    fun createMessageThread(id1: Long, id2: Long) {
        /**
         * Finds people and creates MessageThread
         */

        // Get Person1
        val person1Optional = PERSON_REPO.findById(id1)
        val person1: Person

        if (person1Optional.isEmpty) {
            return
        } else {
            person1 = person1Optional.get()
        }

        // Get Person2
        val person2Optional = PERSON_REPO.findById(id2)
        val person2: Person

        if (person2Optional.isEmpty) {
            return
        } else {
            person2 = person1Optional.get()
        }

        // Create message thread and add people
        val messageThread = MessageThread()
        messageThread.addPeople(person1, person2)

        MESSAGETHREAD_REPO.save(messageThread)
        PERSON_REPO.saveAll(listOf(person1, person2))
    }
}