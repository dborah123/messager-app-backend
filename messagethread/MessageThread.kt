package com.example.messagingapp.messagethread

import com.example.messagingapp.message.Message
import com.example.messagingapp.person.Person
import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*

@Entity
@Table(name = "messagethread")
class MessageThread(
    @Id
    @SequenceGenerator(
        name = "messagethread_sequence",
        sequenceName = "messagethread_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "messagethread_sequence"
    )
    val id: Long? = null,

    @JsonManagedReference
    @ManyToMany(
       fetch = FetchType.LAZY
    )
    @JoinTable(
        name = "people_threads",
        joinColumns = [JoinColumn(name = "thread_id")],
        inverseJoinColumns = [JoinColumn(name = "person_id")]
    )
    val people: MutableSet<Person> = mutableSetOf(),
    @JsonManagedReference
    @OneToMany(
        cascade = [CascadeType.ALL],
        fetch = FetchType.EAGER
    )
    var messages: MutableList<Message> = mutableListOf()

) {

    fun addMessage(message: Message) {
        this.messages.add(message)
    }

    fun addPeople(person1: Person, person2: Person) {
        this.people.addAll(listOf(person1, person2))
    }
}