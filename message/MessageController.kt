package com.example.messagingapp.message

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(path = ["messages"])
class MessageController(
    val MESSAGE_SERVICE: MessageService
) {
    /****************
     * GET MAPPINGS *
     ****************/

    @GetMapping
    fun getMessages(
        @RequestParam(required = false) id: Long?,
        @RequestParam(required = false) threadId: Long?,
        @RequestParam(required = false) content: String?
    ): List<Message> {
        return MESSAGE_SERVICE.getMessages(
            id,
            threadId,
            content
        )
    }

    /*****************
     * POST MAPPINGS *
     *****************/

    @PostMapping("/add/")
    fun addMessage(
        @RequestParam id1: Long,
        @RequestParam id2: Long,
        @RequestParam content: String
    ) {
        MESSAGE_SERVICE.addMessage(id1, id2, content)
    }
}