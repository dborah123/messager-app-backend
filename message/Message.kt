package com.example.messagingapp.message

import com.example.messagingapp.messagethread.MessageThread
import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "message")
class Message(
    @Id
    @SequenceGenerator(
        name = "message_sequence",
        sequenceName = "message_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "message_sequence"
    )
    var id: Long? = null,
    @JsonBackReference
    @ManyToOne
    val thread: MessageThread,
    var content: String,
    val dateTime: LocalDateTime
) {


}