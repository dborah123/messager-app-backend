package com.example.messagingapp.message

import com.example.messagingapp.messagethread.MessageThreadRepository
import com.example.messagingapp.person.Person
import com.example.messagingapp.person.PersonRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class MessageService(
    val MESSAGE_REPO: MessageRepository,
    val PERSON_REPO: PersonRepository,
    val MESSAGETHREAD_REPO: MessageThreadRepository
) {

    private val logger = KotlinLogging.logger {}

    /*****************
     * GET FUNCTIONS *
     *****************/

    fun getMessages(
        id: Long?,
        threadId: Long?,
        content: String?
    ): List<Message> {
        return MESSAGE_REPO.getMessages(
            id,
            threadId,
            content
        )
    }

    /******************
     * POST FUNCTIONS *
     ******************/

    fun addMessage(id1: Long, id2: Long, content: String) {

        // Get person1
        val person1Optional = PERSON_REPO.findById(id1)
        val person1: Person

        if (person1Optional.isEmpty) {
            throw Exception("Person with id1: $id1 cannot be found")
        } else {
            person1 = person1Optional.get()
        }

        // Get person2
        val person2Optional = PERSON_REPO.findById(id1)
        val person2: Person

        if (person2Optional.isEmpty) {
            throw Exception("Person with id2: $id2 cannot be found")
        } else {
            person2 = person2Optional.get()
        }

        // Find thread
        logger.info { "$id1\t$id2" }
        val messagethread = MESSAGETHREAD_REPO.getThreads(
            id = null,
            person1Id = person1,
            person2Id = person2
        )

        // Error check
        if (messagethread.size != 1) {
            throw Exception(
                "Message thread with person1Id $id1 and person2Id $id2 is invalid"
            )
        }

        val message = Message(
            thread = messagethread[0],
            content = content,
            dateTime = LocalDateTime.now()
        )

        messagethread[0].addMessage(message)

        // Save message and update messagethread[0]
        MESSAGE_REPO.save(message)
        MESSAGETHREAD_REPO.save(messagethread[0])
    }
}