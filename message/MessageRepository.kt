package com.example.messagingapp.message

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MessageRepository: JpaRepository<Message, Long> {

    @Query("SELECT m FROM Message m WHERE "
    + "(:id = null OR :id = m.id) "
    + "AND (:threadId = null OR :threadId = m.thread.id) "
    + "AND (:content = null OR :content = m.content)"
    )
    fun getMessages(
        @Param("id") id: Long?,
        @Param("threadId") threadId: Long?,
        @Param("content") content: String?
    ): List<Message>
}