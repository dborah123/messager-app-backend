package com.example.messagingapp.person

import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class PersonService(val PERSON_REPO: PersonRepository) {

    private val logger = KotlinLogging.logger {}

    /*****************
     * GET FUNCTIONS *
     *****************/

    fun getPerson(): List<Person> {
        /**
         * Gets all people in database
         */
        return PERSON_REPO.findAll()
    }

    fun getSpecificPeople(
        id: Long?,
        firstName: String?,
        lastName: String?
    ): List<Person> {
        /**
         * Gets specific people from database (Can be multiple)
         */
        return PERSON_REPO.getSpecificPeople(id, firstName, lastName)
    }

    fun getPerson(id: Long?, firstName: String?, lastName: String?): Person? {
        /**
         * Gets a singular person
         */
        val person = PERSON_REPO.getSpecificPeople(id, firstName, lastName)

        if (person.isEmpty()) {
            return null
        } else if (person.size > 1) {
            logger.error {
                "More than one person with " +
                "id $id, firstname $firstName, and lastname $lastName"
            }
            throw Exception(
                "More than one person with " +
                "id $id, firstname $firstName, and lastname $lastName"
            )
        }

        return person[0]
    }

    /******************
     * POST FUNCTIONS *
     ******************/

    fun addPerson(firstName: String, lastName: String) {
        val person = Person(
            firstName = firstName,
            lastName = lastName
        )

        PERSON_REPO.save(person)
    }

    fun addContact(id: Long, contactId: Long) {

        // Getting person
        val personOptional = PERSON_REPO.findById(id)
        val person: Person

        if (personOptional.isEmpty) {
            throw Exception("Person with id $id is not found")
            return
        } else {
            person = personOptional.get()
        }

        // Getting contact
        val contactOptional = PERSON_REPO.findById(contactId)
        val contact: Person

        if (contactOptional.isEmpty) {
            throw Exception("Person with id $id is not found")
            return
        } else {
            contact = contactOptional.get()
        }

        person.addContact(contact)

        PERSON_REPO.save(person)
    }

    /********************
     * DELETE FUNCTIONS *
     ********************/

    fun deleteContact(id: Long, contactId: Long) {

        // Getting person
        val personOptional = PERSON_REPO.findById(id)
        val person: Person

        if (personOptional.isPresent) {
            person = personOptional.get()
        } else {
            throw Exception("Person with id $id cannot be found")
        }

        // Getting contact to remove
        val contactOptional = PERSON_REPO.findById(contactId)
        val contact: Person

        if (contactOptional.isPresent) {
            contact = contactOptional.get()
        } else {
            throw Exception("Person with id $id cannot be found")
        }

        person.removeContact(contact)

        PERSON_REPO.save(person)
    }

    fun deletePeople(id: Long?, firstName: String?, lastName: String?) {
        if (id == null && firstName == null && lastName == null) {
            throw Exception("id, firstName, and lastName were not specified")
        }

        if (id != null) {
            PERSON_REPO.deleteById(id)
        }

        if (firstName != null || lastName != null) {
            val people = PERSON_REPO.getSpecificPeople(
                id = null,
                firstName = firstName,
                lastName = lastName
            )

            PERSON_REPO.deleteAll(people)
        }
    }
}