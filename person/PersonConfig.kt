package com.example.messagingapp.person

import com.example.messagingapp.message.Message
import com.example.messagingapp.message.MessageRepository
import com.example.messagingapp.messagethread.MessageThread
import com.example.messagingapp.messagethread.MessageThreadRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDateTime

@Configuration
class PersonConfig {

    @Bean
    fun run(
        USER_REPO: PersonRepository,
        MESSAGETHREAD_REPO: MessageThreadRepository,
        MESSAGE_REPO: MessageRepository
    ): CommandLineRunner {
        return CommandLineRunner { _ ->
            val user1 = Person(
                firstName = "Dan",
                lastName = "Borah",
            )

            val user2 = Person(
                firstName = "Rasika",
                lastName = "Iyer"
            )

            val thread1 = MessageThread()

            val message1 = Message(
                thread = thread1,
                content = "Hello!",
                dateTime = LocalDateTime.now()
            )

            thread1.people.add(user1)
            thread1.people.add(user2)
            thread1.addMessage(message1)

            user1.threads.add(thread1)
            user2.threads.add(thread1)

            USER_REPO.saveAll(listOf(user1, user2))
            MESSAGETHREAD_REPO.save(thread1)
            MESSAGE_REPO.save(message1)
        }

    }
}